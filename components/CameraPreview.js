import React, { useState, useEffect } from 'react';
import { Camera } from 'expo-camera';
import { View, Text } from 'react-native';


const CameraPreview = () => {
  const [hasCameraPermission, setHasCameraPermission] = useState(null);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasCameraPermission(status === 'granted');
    })();
  }, []);

  if (hasCameraPermission === null) {
    return <Text>Requesting camera permission...</Text>;
  }
  if (hasCameraPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={{ flex: 1 }}>
      <Text>asdsadaaaa</Text>
      <Camera style={{ flex: 1 }} />
    </View>
  );
};

export default CameraPreview;