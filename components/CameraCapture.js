import React, { useState, useRef } from 'react';
import { Camera, Image } from 'expo-camera';
import { View } from 'react-native';

const CameraCapture = () => {
  const cameraRef = useRef(null);
  const [image, setImage] = useState(null);

  const takePhoto = async () => {
    if (!cameraRef.current) return;

    const { uri } = await cameraRef.current.takePhotoAsync();
    setImage(uri);
  };

  return (
    <View>
      <Camera ref={cameraRef} />
      {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
      <Button title="Tirar Foto" onPress={takePhoto} />
    </View>
  );
};

export default CameraCapture;
