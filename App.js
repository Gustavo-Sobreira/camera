import React, { useState, useEffect } from 'react';
import { View, Text, Button } from 'react-native';
import { Camera } from 'expo-camera';
import { Audio } from 'expo-av';
import * as MediaLibrary from 'expo-media-library';


function App() {
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [cameraRef, setCameraRef] = useState(null);
  const [isRecording, setIsRecording] = useState(false);
  const [videoUri, setVideoUri] = useState(null);
  const [videoAssets, setVideoAssets] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    (async () => {
      setLoading(true);
      try {
        const [cameraPermission, mediaPermission, audioPermission] = await Promise.all([
          Camera.requestCameraPermissionsAsync(),
          MediaLibrary.requestPermissionsAsync(),
          Audio.requestPermissionsAsync()
        ]);

        console.log('-------------------');
        if (cameraPermission.status !== 'granted') {
          console.log("ERRO  1");
          throw new Error('Sem acesso à câmera');
        }
        console.log("📷 - PERMISSÕES OK");


        if (mediaPermission.status !== 'granted') {
          console.log("ERRO  2");
          throw new Error('Sem acesso à galeria de mídia');
        }
        console.log("📁 - PERMISSÕES OK");

        if (audioPermission.status !== 'granted') {
          console.log("ERRO  3");
          throw new Error('Sem acesso ao microfone');
        }
        console.log("🎤 - PERMISSÕES OK");
        
        setHasCameraPermission(true);
        
        const media = await MediaLibrary.getAssetsAsync({ mediaType: 'video' });
        setVideoAssets(media.assets);
        console.log('-------------------');
      } catch (error) {
        setError(error.message);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const takeVideo = async () => {
    console.log('take video')
    if(cam){
      setRecord(true)
      let video = await cam.recordAsync({mute:true, maxDuration:5})
      console.log('video', video)  
    }
  }

  const gravar = async () => {
    if (cameraRef) {
      console.log('-> CLICOU EM GRAVAR 📷');
      if (isRecording) {
        console.log("-> GRAVAÇÃO EM ANDAMENTO 🏃");
        return;
      }

      try {
        console.log("-> LIGANDO CAMÊRA 🔌");
        setIsRecording(true);
        const { uri } = await cameraRef.recordAsync({mute:false, maxDuration:5});
        setVideoUri(uri);
        console.log("-> LIGADA ⚡" + uri);
      } catch (error) {
        console.log("-> ERRO AO LIGAR A CÂMERA 🪦" + "\n" + error.message);
        setError(error.message);
      }
    }
  };

  const salvar = async () => {
    if (cameraRef && isRecording) {
      try {
        // const { uri } = await cameraRef.stopRecording();
        setIsRecording(false); 
        const asset = await MediaLibrary.createAssetAsync(videoUri);
        setVideoAssets([...videoAssets, asset]);
        console.log("📁 - SALVO");
      } catch (error) {
        console.log(error.message);
        setError(error.message);
      }
    } else {
      console.log('-> CLICOU EM SALVAR, MAS NÃO HÁ GRAVAÇÃO 🛑');
    }
  };
  

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      {loading ? (
        <Text>Carregando...</Text>
      ) : hasCameraPermission === null ? (
        <Text>Aguardando permissões...</Text>
      ) : hasCameraPermission === false ? (
        <Text>Sem acesso à câmera</Text>
      ) : error ? (
        <Text>Erro: {error}</Text>
      ) : (
        <View style={{ flex: 1, width: '100%' }}>
          <Camera
            style={{ flex: 1 }}
            ref={(ref) => setCameraRef(ref)}
            type={Camera.Constants.Type.back}
            flashMode={'on'}
          />
          <View style={{ position: 'absolute', bottom: 20, flexDirection: 'row', justifyContent:'space-around',  width: '100%' }}>
            <Button title="Iniciar" onPress={gravar} color={isRecording ? 'green' : 'red'}/>
            <Button title="Salvar" onPress={salvar} />
          </View>
        </View>
      )}
    </View>
  );
}

export default App;